import datetime

from django.db.models import Count, F, FloatField, Q, Sum
from django.http import JsonResponse
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import (AllowAny, IsAuthenticated,
                                        IsAuthenticatedOrReadOnly)
from rest_framework.response import Response

from lagarosa.permissions import IsOwnerOrReadOnly
from lagarosa.utils import create_and_send_notification
from users.models import CustomUser

from .data import ORDER_CREATED, ORDER_FINISHED, WOMPI_STATUS
from .models import (Audit, City, Inventory, Notification, Order, OrderDetail,
                     Product, ProductCategory, ProductFlavor, SalesPoint,
                     Seating, StatusOrder)
from .payments import Wompi
from .serializers import (AuditSerializer, CitySerializer, InventorySerializer,
                          NotificationSerializer, OrderDetailSerializer,
                          OrderSerializer, ProductCategorySerializer,
                          ProductFlavorSerializer, ProductSerializer,
                          SalesPointSerializer, SeatingSerializer)
from lagarosa.utils import create_and_send_notification


class SalesPointFrontViewSet(viewsets.ModelViewSet):
    serializer_class = SalesPointSerializer
    parser_classes = (MultiPartParser, FormParser)
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        queryset = SalesPoint.objects.all()
        is_active = self.request.query_params.get('is_active', None)
        if (is_active):
            return queryset.filter(is_active=is_active)
        return queryset


class SeatingViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Seating.objects.all()
    serializer_class = SeatingSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        sale_point = self.request.query_params.get('sale_point', None)
        return Seating.objects.filter(sale_point=sale_point)


class ProductCategoryFrontViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ProductCategory.objects.filter(
        is_active=True).order_by('sort_number')
    serializer_class = ProductCategorySerializer


class ProductFlavorFrontViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = ProductFlavor.objects.filter(is_active=True)
    serializer_class = ProductFlavorSerializer


class CityViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = City.objects.filter(is_active=True)
    serializer_class = CitySerializer
    permission_classes = [IsAuthenticated]


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    parser_classes = (MultiPartParser, FormParser)
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class InventoryViewSet(viewsets.ModelViewSet):
    serializer_class = InventorySerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        queryset = Inventory.objects.all()
        sale_point = self.request.query_params.get('sale_point', None)

        if sale_point:
            return queryset.filter(is_active=True, sale_point=sale_point).order_by('sort_number')
        return queryset

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def destroy(self, *args, **kwargs):
        serializer = self.get_serializer(self.get_object())
        super().destroy(*args, **kwargs)
        return Response(serializer.data, status=status.HTTP_200_OK)


class OrderViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]
    queryset = Order.objects.all()

    def get_queryset(self):
        request = self.request
        is_active = request.query_params.get('order_active', None)
        status = request.query_params.get('status', None)
        user_id = request.query_params.get('user_id', None)
        transaction_id = request.query_params.get('transaction_id', None)
        instant_delivery = request.query_params.get('instant_delivery', True)

        if is_active:
            return Order.objects.filter(Q(status=1) | Q(status=2) |
                                        Q(status=3), user=request.user.id)
        if status:
            if status == '2':
                return Order.objects.filter(Q(status=2) | Q(status=3),
                                            sale_point=request.user.sale_point)
            if instant_delivery == '1':
                instant_delivery = False
                
            return Order.objects.filter(status=status,
                                        sale_point=request.user.sale_point, delivery_date__isnull=instant_delivery)
        if user_id:
            return Order.objects.filter(user=user_id)
        if transaction_id:
            return Order.objects.filter(wompi_transaction_id=transaction_id)

        return (Order.objects.filter(sale_point=request.user.sale_point)
                if request.user.is_staff | request.user.is_superuser
                else Order.objects.filter(user=request.user.id))

    def perform_create(self, serializer):
        user = self.request.user
        try:
            client = self.request.data['client']
        except:
            client = None
        if client:
            user = CustomUser.objects.get(pk=client)
        serializer.save(user=user)


class OrderDetailViewSet(mixins.DestroyModelMixin, viewsets.GenericViewSet):
    serializer_class = OrderDetailSerializer
    permission_classes = [IsAuthenticated]
    queryset = OrderDetail.objects.all()

    def destroy(self, *args, **kwargs):
        serializer = self.get_serializer(self.get_object())
        order_id = serializer.data['order_id']
        product_price = serializer.data['unit_price'] * \
            serializer.data['quantity']
        order = Order.objects.get(pk=order_id)
        order.total_price = order.total_price - product_price
        order.save()
        super().destroy(*args, **kwargs)
        return Response(serializer.data, status=status.HTTP_200_OK)


class StatisticViewSet(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]

    def list(self, request):
        user = request.user

        start_date = datetime.datetime.today(
        ) - datetime.timedelta(days=datetime.datetime.today().weekday() % 7)

        end_date = datetime.datetime.today(
        ) + datetime.timedelta(days=datetime.datetime.today().weekday() % 7)

        top_clients = Order.objects.filter(
            status=ORDER_FINISHED,
            created_at__range=[start_date, end_date],
            sale_point=user.sale_point
        ).values(
            'user__first_name',
            'user'
        ).annotate(
            total=Count('user')
        ).order_by('-total')[:5]

        top_sales = OrderDetail.objects.filter(
            order__status=ORDER_FINISHED,
            order__created_at__range=[start_date, end_date],
            order__sale_point=user.sale_point
        ).values(
            'product__name'
        ).annotate(
            total=Sum(F('unit_price')*F('quantity'), output_field=FloatField())
        ).order_by('-total')[:10]

        top_payment_method = Order.objects.filter(
            status=ORDER_FINISHED,
            created_at__range=[start_date, end_date],
            sale_point=user.sale_point
        ).values('payment_method').annotate(
            total=Count('payment_method')).order_by('-total')[:1]

        order_history = Order.objects.filter(
            status=ORDER_FINISHED, created_at__range=[start_date, end_date],
            sale_point=user.sale_point
        ).values(
            'created_at__date', 'order_type'
        ).annotate(
            total=Count('created_at')
        ).order_by(
            'created_at__date', 'order_type')

        try:
            client_one = top_clients[0]['user']
        except:
            client_one = None
        try:
            client_two = top_clients[1]['user']
        except:
            client_two = None
        try:
            client_three = top_clients[2]['user']
        except:
            client_three = None

        best_clients = OrderDetail.objects.filter(
            Q(order__user=client_one) | Q(
                order__user=client_two) | Q(order__user=client_three),
            order__status=ORDER_FINISHED,
            order__sale_point=user.sale_point,
            order__created_at__range=[start_date, end_date]
        ).values(
            'product__name',
            'order__user__first_name'
        ).annotate(
            total=Sum('quantity')
        ).order_by('-total')

        return Response({
            "top_sales": top_sales,
            "top_clients": top_clients,
            "top_payment_method": top_payment_method,
            "order_history": order_history,
            "best_clients": best_clients
        })


class AuditViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Audit.objects.all()
    serializer_class = AuditSerializer
    permission_classes = [IsAuthenticated]


class WompiViewSet(viewsets.ViewSet):
    permission_classes = [IsAuthenticated]

    @action(methods=['get'], detail=False, permission_classes=(AllowAny,))
    def token_acceptance(self, request):
        wompi = Wompi()
        return Response(wompi.get_acceptance_token())

    @action(methods=['get'], detail=False, permission_classes=(AllowAny,))
    def banks(self, request):
        wompi = Wompi()
        return Response(wompi.get_banks())

    @action(methods=['post'], detail=False, permission_classes=(AllowAny,))
    def events(self, request):

        try:
            wompi_data = request.data['data']
            transaction_type = request.data['event']
        except:
            return Response("Invalid Request 1",
                            status=status.HTTP_400_BAD_REQUEST)

        wompi_status = wompi_data['transaction']['status']
        if transaction_type == 'transaction.updated':
            transaction_id = wompi_data['transaction']['id']
            try:
                order = Order.objects.get(
                    wompi_transaction_id=transaction_id)

                print(wompi_status)
                print(WOMPI_STATUS[wompi_status])
                order.status = StatusOrder.objects.get(
                    pk=WOMPI_STATUS[wompi_status])
                order.save()
                serializer = OrderSerializer(order)

                if order.status.id == 1:
                    create_and_send_notification(order, True)
                return Response(serializer.data)

            except Order.DoesNotExist:
                return Response("Order Does not exist",
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response("Invalid Request 2",
                            status=status.HTTP_400_BAD_REQUEST)


class NotificationViewSet(mixins.ListModelMixin,
                          mixins.UpdateModelMixin,
                          viewsets.GenericViewSet):
    serializer_class = NotificationSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        sale_point = user.sale_point.id
        return Notification.objects.filter(sale_point=sale_point)


'''
class TestViewSet(viewsets.ViewSet):
    @action(methods=['get'], detail=False)
    def test(self, request):
        order = Order.objects.first()
        create_and_send_notification(order, True)
        return Response({'success': True, 'data': 'test', 'group': str(order.sale_point.id)})
'''

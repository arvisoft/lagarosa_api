def save_audit(sender, instance, created, **kwargs):
    from .models import Audit, Order, Inventory, SalesPoint, Product, OrderDetail
    from users.models import CustomUser
    if created:
        if isinstance(instance, CustomUser):
            name = f'{instance.first_name} {instance.last_name}'
            if instance.is_staff:
                Audit.objects.create(
                    message=f'Usuario de equipo creado: {name}')
        if isinstance(instance, Order):
            order = instance.id
            user = instance.user.first_name
            if instance.status.id == 1 or instance.status.id == 2:
                Audit.objects.create(message=f'{user} creó orden #{order}')

            if instance.order_type == 1 and instance.status.id == 1:
                from lagarosa.utils import create_and_send_notification
                create_and_send_notification(instance, True)
        if isinstance(instance, OrderDetail):
            order = instance.order
            if order.status.id == 1 or order.status.id == 2:
                sale_point = order.sale_point.id
                actual_product = instance.product.id
                quantity = instance.quantity

                if instance.product.product_type == 'BS':
                    inv = Inventory.objects.get(
                        sale_point=sale_point, product=actual_product)

                    if inv.in_stock != -1000:
                        inv.in_stock = inv.in_stock - quantity
                        inv.save()

                if instance.product.product_type == 'CB':
                    for item in instance.product_configs:
                        inv = Inventory.objects.get(
                            sale_point=sale_point, product=item['id'])
                        if inv.in_stock != -1000:
                            inv.in_stock = inv.in_stock - \
                                (int(item['quantity']) * quantity)
                            inv.save()

        if isinstance(instance, Inventory):
            inventory = f'{instance.product.name} ({instance.in_stock} unds)'
            sale_point = f'en punto de Venta: {instance.sale_point.name}'
            user = instance.user.first_name
            Audit.objects.create(
                message=f'{user} agrego inventario: {inventory} {sale_point}')
        if isinstance(instance, SalesPoint):
            name = instance.name
            Audit.objects.create(message=f'Punto de venta creado: {name}')
        if isinstance(instance, Product):
            name = instance.name
            user = instance.user.first_name
            Audit.objects.create(message=f'{user} creó un producto: {name}')


def send_status_order_email(sender, instance, **kwargs):
    if not instance._state.adding:
        from lagarosa.utils import EmailThread
        email = instance.direct_email or instance.user.email
        print(email)
        if instance.status.id == 2 or instance.status.id == 3:
            EmailThread(
                f'Su Pedido  #{instance.id} se ha actualizado',
                email,
                'orders/status_email.html',
                {'order': instance}).start()

import datetime
import json
import uuid

from rest_framework import serializers
from rest_framework.response import Response

from lagarosa.utils import Base64ImageField
from users.models import Address, CustomUser

from .data import WOMPI_PENDING
from .models import (Audit, City, Inventory, Notification, Order, OrderDetail,
                     Product, ProductCategory, ProductDetail, ProductFlavor,
                     ProductImage, ProductIngredient, SalesPoint,
                     SalesPointSchedule, Seating, StatusOrder)
from .payments import Wompi


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = CustomUser
        exclude = ('password',)


class AuditSerializer(serializers.ModelSerializer):
    class Meta:
        model = Audit
        fields = '__all__'


class AddressSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.id')

    class Meta:
        model = Address
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id', 'name')


class SalesPointScheduleASerializer(serializers.ModelSerializer):
    class Meta:
        model = SalesPointSchedule
        fields = '__all__'
        read_only_fields = ['sale_point']


class SeatingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seating
        fields = '__all__'
        read_only_fields = ['sale_point']


class SalesPointSerializer(serializers.ModelSerializer):
    schedules = SalesPointScheduleASerializer(many=True, read_only=True)
    seatings = SeatingSerializer(many=True, read_only=True)
    cities = CitySerializer(many=True, read_only=True)
    thumbnail = Base64ImageField()

    class Meta:
        model = SalesPoint
        fields = '__all__'
        depth = 1

    def create(self, validated_data):
        request = self.context.get('request')
        data = request.data.copy()
        city_id = data.pop('city')[0]
        seatings = data.pop('seatings')[0]
        schedules_data = json.loads(data.pop('schedules')[0])
        validated_data['city'] = City.objects.get(pk=city_id)
        sale_point = SalesPoint.objects.create(**validated_data)
        sale_point.save()
        for shedule_data in schedules_data:
            SalesPointSchedule.objects.create(
                sale_point=sale_point, **shedule_data)
        for i in range(1, int(seatings)+1):
            Seating.objects.create(
                sale_point=sale_point, **{"number": i}
            )
        return sale_point

    def update(self, instance, validated_data):
        request = self.context.get('request')
        data = request.data
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get(
            'description', instance.description)
        instance.address = validated_data.get('address', instance.address)
        instance.latitude = validated_data.get('latitude', instance.latitude)
        instance.longitude = validated_data.get(
            'longitude', instance.longitude)
        instance.is_principal = validated_data.get(
            'is_principal', instance.is_principal)
        instance.thumbnail = validated_data.get(
            'thumbnail', instance.thumbnail)
        instance.is_active = validated_data.get(
            'is_active', instance.is_active)
        instance.day_message = validated_data.get(
            'day_message', instance.day_message)
        try:
            city_id = data['city']
        except:
            city_id = None

        if city_id is not None:
            validated_data['city'] = City.objects.get(pk=city_id)
            instance.city = validated_data.get('city', instance.city)

        try:
            number_of_seatings = int(data['seatings'])
        except:
            number_of_seatings = None

        if number_of_seatings is not None:
            seatings = Seating.objects.filter(sale_point=instance)
            current_seatings = seatings.count()
            if number_of_seatings > current_seatings:
                for i in range(current_seatings + 1, number_of_seatings + 1):
                    Seating.objects.create(
                        sale_point=instance, **{"number": i}
                    )
            elif number_of_seatings < current_seatings:
                index = seatings.count() - number_of_seatings
                for seat in seatings[number_of_seatings-1:current_seatings-1]:
                    seat.delete()
        try:
            schedules_data = json.loads(data['schedules'])
        except:
            schedules_data = None

        if schedules_data is not None:
            for schedule in schedules_data:
                SalesPointSchedule.objects.filter(
                    pk=schedule['id'], sale_point=instance).update(**schedule)

        instance.save()
        return instance


class ProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategory
        fields = '__all__'


class ProductFlavorSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductFlavor
        fields = '__all__'


class ProductImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage
        fields = ('id', 'path', 'product')


class ProductDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductDetail
        fields = ('child_product',)
        depth = 1


class ProductDetailListingField(serializers.RelatedField):
    def to_representation(self, value):
        return {
            "id": value.child_product.id,
            "name": value.child_product.name,
            "price": value.child_product.price
        }


class ProductIngredientSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductIngredient
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    images = ProductImageSerializer(many=True, read_only=True)
    parent_products = ProductDetailListingField(many=True, read_only=True)
    ingredients = ProductIngredientSerializer(many=True, read_only=True)
    category = ProductCategorySerializer(many=True, read_only=True)
    flavors = ProductFlavorSerializer(many=True, read_only=True)

    class Meta:
        model = Product
        exclude = ['user', 'created_at', 'updated_at']
        depth = 2

    def create(self, validated_data):
        request = self.context.get('request')
        data = request.data
        images_data = data.pop('images')
        flavor_id = data.pop('flavor')[0]
        validated_data['flavor'] = ProductFlavor.objects.get(pk=flavor_id)
        categories = json.loads(data.pop('category')[0])
        product = Product.objects.create(**validated_data)
        for category in categories:
            c = ProductCategory.objects.get(pk=category)
            product.category.add(c)
        product.save()
        for image in images_data:
            image_obj = {"path": image}
            ProductImage.objects.create(
                product=product, **image_obj)

        product_type = validated_data['product_type']

        if product_type == 'CB':
            product_details_data = json.loads(
                data.pop('product_details')[0])
            for detail in product_details_data:
                ProductDetail.objects.create(
                    parent_product=product,
                    child_product=Product.objects.get(
                        pk=detail['child_product'])
                )

        if product_type == 'PE':
            ingredients_data = json.loads(data.pop('ingredients')[0])
            for ingredient in ingredients_data:
                ProductIngredient.objects.create(
                    product=product, **ingredient
                )
        return product

    def update(self, instance, validated_data):
        request = self.context.get('request')
        data = request.data
        instance.name = validated_data.get('name', instance.name)
        instance.description = validated_data.get(
            'description', instance.description)
        instance.product_type = validated_data.get(
            'product_type', instance.product_type)
        instance.unit_limit = validated_data.get(
            'unit_limit', instance.unit_limit)
        instance.restriction_message = validated_data.get(
            'restriction_message', instance.restriction_message)
        instance.price = validated_data.get('price', instance.price)
        instance.is_active = validated_data.get(
            'is_active', instance.is_active)

        try:
            flavor_id = data.pop('flavor')[0]
        except:
            flavor_id = None

        if flavor_id is not None:
            validated_data['flavor'] = ProductFlavor.objects.get(pk=flavor_id)
            instance.flavor = validated_data.get('flavor', instance.flavor)

        try:
            images_data = data.pop('images')
        except:
            images_data = None

        if images_data is not None:
            for image in images_data:
                image_obj = {"path": image}
                ProductImage.objects.create(product=instance, **image_obj)

        try:
            categories = json.loads(data['category'])        
        except:
            categories = None

        if categories is not None:
            instance.category.clear()
            for category in categories:
                c = ProductCategory.objects.get(pk=category)
                instance.category.add(c)

        try:
            deleted_images = json.loads(data.pop('deleted_images')[0])
        except:
            deleted_images = None

        if deleted_images is not None:
            for image_id in deleted_images:
                ProductImage.objects.filter(pk=image_id).delete()
        try:
            product_type = validated_data['product_type']
        except:
            product_type = None

        if product_type == 'CB':

            try:
                product_details_data = json.loads(
                    data.pop('product_details')[0])
            except:
                product_details_data = None

            if product_details_data is not None:
                ProductDetail.objects.filter(parent_product=instance).delete()
                for detail in product_details_data:
                    ProductDetail.objects.create(
                        parent_product=instance,
                        child_product=Product.objects.get(
                            pk=detail['child_product'])
                    )

        if product_type == 'PE':
            try:
                ingredients_data = json.loads(data.pop('ingredients')[0])
            except:
                ingredients_data = None

            if ingredients_data is not None:
                ProductIngredient.objects.filter(
                    product=instance).delete()
                for ingredient in ingredients_data:
                    ProductIngredient.objects.create(
                        product=instance, **ingredient
                    )

        instance.save()
        return instance


class InventorySerializer(serializers.ModelSerializer):
    product = ProductSerializer(read_only=True)
    user = serializers.ReadOnlyField(source='user.id')
    sort_number = serializers.ReadOnlyField()

    class Meta:
        model = Inventory
        exclude = ['created_at', 'updated_at']
        depth = 2

    def create(self, validated_data):
        request = self.context.get('request')
        data = request.data
        products = data.pop('products')
        sale_point_id = data.pop('sale_point')
        sale_point = SalesPoint.objects.get(pk=sale_point_id)
        user = request.user

        for product_data in products:
            product = Product.objects.get(pk=product_data['product'])
            sort = product_data['sort_number']
            inventory_item = {"product": product,
                              "sale_point": sale_point, "user": user, "sort_number": sort}
            inventory = Inventory.objects.create(**inventory_item)

        return inventory

    def update(self, instance, validated_data):
        request = self.context.get('request')
        data = request.data

        try:
            validated_data['sort_number'] = data['sort_number']
        except:
            pass

        instance.sort_number = validated_data.get(
            'sort_number', instance.sort_number)
        instance.in_stock = validated_data.get('in_stock', instance.in_stock)
        instance.min_units = validated_data.get(
            'min_units', instance.min_units)

        instance.save()
        return instance


class ProductCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCategory
        fields = ('id', 'category')


class ProductFlavorSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductFlavor
        fields = '__all__'


class StatusOrderSerializar(serializers.SlugRelatedField):

    def to_representation(self, value):
        return {
            "id": value.id,
            "status": value.status
        }


class OrderDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetail
        fields = '__all__'
        read_only_fields = ['order']

    def to_representation(self, value):
        return {
            "id": value.id,
            "quantity": value.quantity,
            "product": value.product.name,
            "product_id": value.product.id,
            "product_type": value.product.product_type,
            "unit_price": value.unit_price,
            "product_configs": value.product_configs,
            "order_id": value.order.id
        }


class OrderSerializer(serializers.ModelSerializer):
    status = StatusOrderSerializar(
        slug_field='id',  queryset=StatusOrder.objects.all())
    order_details = OrderDetailSerializer(many=True)
    user = UserSerializer(read_only=True)
    sale_point = SalesPointSerializer(read_only=True)
    address = AddressSerializer(read_only=True)

    class Meta:
        model = Order
        fields = '__all__'
        depth: 1

    def create(self, validated_data):
        request = self.context.get('request')
        data = request.data
        payment_method = validated_data['payment_method']
        reference = uuid.uuid4()
        validated_data['order_uuid'] = reference
        sale_point_id = data.pop('sale_point')
        validated_data['sale_point'] = SalesPoint.objects.get(
            pk=sale_point_id)
        try:
            address_id = data.pop('address')
            validated_data['address'] = Address.objects.get(pk=address_id)
        except:
            pass

        try:
            guest_data = data.pop('guest_data')
            guest_address = guest_data['guest_address']
            validated_data['address'] = Address.objects.create(
                                            user=request.user,
                                            description='Dirección Anonima',
                                            **guest_address)
            validated_data['direct_client'] = guest_data['guest_name']
            validated_data['direct_phone'] = guest_data['guest_phone']
            validated_data['direct_email'] = guest_data['guest_email']
            
        except:
            pass

        order_detail_data = validated_data.pop('order_details')
        self.validate_units(sale_point_id, order_detail_data)
        # Request to wompi
        if payment_method != 1 and payment_method != 2 and payment_method != 7:
            payment_data = data.pop('payment_data')
            payment_data['email'] = request.user.email
            payment_data['reference'] = str(reference)
            wompi = Wompi()
            wompi_response = wompi.process_payment(payment_data)
            if "data" in wompi_response.keys():
                wompi_data = wompi_response['data']
                validated_data['wompi_transaction_id'] = wompi_data['id']

                if payment_method == 4:
                    pse_data = wompi_data['payment_method']['extra']
                    if pse_data['return_code'] == "SUCCESS":
                        validated_data['pse_link'] = pse_data['async_payment_url']
                        validated_data['status'] = StatusOrder.objects.get(
                            pk=WOMPI_PENDING)
                    else:
                        raise serializers.ValidationError(wompi_response)
                if payment_method == 5:
                    validated_data['status'] = StatusOrder.objects.get(
                        pk=WOMPI_PENDING)
                if payment_method == 6:
                    bc_button_data = wompi_data['payment_method']['extra']
                    validated_data['pse_link'] = bc_button_data['async_payment_url']
                    validated_data['status'] = StatusOrder.objects.get(
                        pk=WOMPI_PENDING)
            else:
                raise serializers.ValidationError(wompi_response)

        order = Order.objects.create(**validated_data)
        for od_data in order_detail_data:
            OrderDetail.objects.create(order=order, **od_data)

        return order

    def update(self, instance, validated_data):
        request = self.context.get('request')
        data = request.data

        try:
            change_status = data['change_status']
        except:
            change_status = False

        if ((instance.order_type != 1 or instance.status.id != 1) and change_status is True):
            raise serializers.ValidationError(
                "Pedido en proceso, no se puede modificar!")

        instance.total_price = validated_data.get(
            'total_price', instance.total_price)
        instance.delivery_name = validated_data.get(
            'delivery_name', instance.delivery_name)
        instance.message = validated_data.get(
            'message', instance.message)
        instance.rating = validated_data.get(
            'rating', instance.rating)
        instance.comment = validated_data.get(
            'comment', instance.comment)
        instance.annotations = validated_data.get(
            'annotations', instance.annotations)
        instance.payment_method = validated_data.get(
            'payment_method', instance.payment_method)
        instance.direct_client = validated_data.get(
            'direct_client', instance.direct_client)
        instance.seating_number = validated_data.get(
            'seating_number', instance.seating_number)
        instance.status = validated_data.get(
            'status', instance.status)
        instance.delivery_date = validated_data.get(
            'delivery_date', instance.delivery_date)

        try:
            p_minutes = data['preparation_minutes']
            p_time = datetime.datetime.now() + datetime.timedelta(
                minutes=p_minutes)
            instance.preparation_time = p_time.time()
        except:
            pass

        try:
            d_minutes = data['delivery_minutes']
            d_time = datetime.datetime.now() + datetime.timedelta(
                minutes=d_minutes)
            instance.delivery_time = d_time.time()
        except:
            pass

        try:
            order_detail_data = validated_data.pop('order_details')
        except:
            order_detail_data = []

        for od_data in order_detail_data:
            od_data['order'] = instance
            try:
                detail_id = int(data['order_details'][0]['id'])
            except:
                detail_id = None
            obj, created = OrderDetail.objects.update_or_create(
                id=detail_id, defaults=od_data)

        instance.save()
        return instance

    def validate_units(self, sale_point, product_list):
        errors = []
        for item in product_list:
            product = item['product']

            if product.product_type == 'BS':
                inv = Inventory.objects.get(
                    sale_point=sale_point, product=product.id)
                in_stock = inv.in_stock
                stock = in_stock - item['quantity']
                if stock < 0 and in_stock != -1000:
                    errors.append(f'{product.name} - {inv.in_stock}')

            if product.product_type == 'CB':
                errors_cb = ""
                for pc in item['product_configs']:
                    inv = Inventory.objects.get(
                        sale_point=sale_point, product=pc['id'])
                    in_stock = inv.in_stock
                    stock = in_stock - \
                        (int(pc['quantity']) * item['quantity'])
                    if stock < 0 and in_stock != -1000:
                        errors_cb = errors_cb + \
                            (f'{pc["name"]} - {inv.in_stock},')
                if len(errors_cb) > 0:
                    errors.append({product.name: errors_cb})

        if len(errors) > 0:
            raise serializers.ValidationError({'out_of_stock': errors})


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = '__all__'

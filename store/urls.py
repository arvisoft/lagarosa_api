from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import (AuditViewSet, CityViewSet, InventoryViewSet,
                    NotificationViewSet, OrderDetailViewSet, OrderViewSet,
                    ProductCategoryFrontViewSet, ProductFlavorFrontViewSet,
                    ProductViewSet, SalesPointFrontViewSet, SeatingViewSet,
                    StatisticViewSet, WompiViewSet)

router = DefaultRouter(trailing_slash=False)
router.register(r'api/store/sales-point',
                SalesPointFrontViewSet, basename='store')
router.register(r'api/store/city',
                CityViewSet, basename='store')
router.register(r'api/store/seating', SeatingViewSet, basename='store')
router.register(r'api/store/product-category',
                ProductCategoryFrontViewSet, basename='store')
router.register(r'api/store/product-flavor',
                ProductFlavorFrontViewSet, basename='store')
router.register(r'api/store/product',
                ProductViewSet, basename='store')
router.register(r'api/store/order',
                OrderViewSet, basename='store')
router.register(r'api/store/orderdetail',
                OrderDetailViewSet, basename='store')
router.register(r'api/store/inventory',
                InventoryViewSet, basename='store')
router.register(r'api/store/statistic',
                StatisticViewSet, basename='store')
router.register(r'api/store/audit',
                AuditViewSet, basename='store')
router.register(r'api/wompi',
                WompiViewSet, basename='store')
router.register(r'api/notifications',
                NotificationViewSet, basename='store')
#router.register(r'api/test', TestViewSet, basename='test')

urlpatterns = [
    path('', include(router.urls))
]

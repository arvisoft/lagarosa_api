
PRODUCT_TYPES = [
    ('BS', 'Basico'),
    ('CB', 'Combo'),
    ('PE', 'Personalizado'),
]

DAYS_OF_WEEK = [
    (1, 'Lunes'),
    (2, 'Martes'),
    (3, 'Miercoles'),
    (4, 'Jueves'),
    (5, 'Viernes'),
    (6, 'Sabado'),
    (7, 'Domingo')
]

ORDERS_TYPES = [
    (1, 'Web'),
    (2, 'Punto Local'),
    (3, 'Punto Domicilio')
]

INGREDIENTS_CHOICES = [
    (1, 'Selección Unica'),
    (2, 'Selección Multiple')
]

PAYMENTS_METHODS = [
    (1, 'Efectivo'),
    (2, 'Datafono'),
    (3, 'CARD'),
    (4, 'PSE'),
    (5, 'NEQUI'),
    (6, 'BANCOLOMBIA_TRANSFER'),
    (7, 'BANCOLOMBIA QR')
]

WOMPI_STATUS = {
    'APPROVED': 1,
    'PENDING': 6,
    'VOIDED': 7,
    'DECLINED': 8,
    'ERROR': 9
}

ORDER_CREATED = 1
ORDER_IN_PROGRESS = 2
ORDER_IN_DELIVERY = 3
ORDER_FINISHED = 4
ORDER_CANCELLED = 5
WOMPI_PENDING = 6


import requests
import time
from requests.exceptions import HTTPError
from .data import PAYMENTS_METHODS
from decouple import config


class Wompi():

    def __init__(self):
        self.pub_api_key = config('WOMPI_PUBLIC_KEY')
        self.base_url = config('WOMPI_URL')
        self.headers = {'Authorization': f'Bearer {self.pub_api_key}',
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'}

    def get_acceptance_token(self):
        try:
            response = requests.get(
                f'{self.base_url}merchants/{self.pub_api_key}')
            response.raise_for_status()
        except HTTPError as http_err:
            return f'HTTP error occurred: {http_err}'
        else:
            json_response = response.json()
            data = {
                "accepted_payment_methods": json_response["data"].pop("accepted_payment_methods"),
                "presigned_acceptance": json_response["data"].pop("presigned_acceptance")
            }
            return data

    def tokenize_card(self, data):
        response = requests.post(f'{self.base_url}tokens/cards',
                                 json=data,
                                 headers=self.headers)
        return response.json()

    def create_transaction(self, data):
        response = requests.post(f'{self.base_url}transactions',
                                 json=data,
                                 headers=self.headers)
        return response.json()

    def get_transaction_status(self, transaction_id):
        try:
            response = requests.get(
                f'{self.base_url}transactions/{transaction_id}',
                headers=self.headers)
            response.raise_for_status()
        except HTTPError as http_err:
            return f'HTTP error occurred: {http_err}'
        else:
            return response.json()

    def get_banks(self):
        response = requests.get(
            f'{self.base_url}pse/financial_institutions',
            headers=self.headers)
        return response.json()

    def process_payment(self, data):

        transaction_data = {
            "acceptance_token": data['acceptance_token'],
            "amount_in_cents": data['amount_in_cents'],
            "currency": "COP",
            "customer_email": data["email"],
            "reference": data["reference"]
        }
        payment_method = data['payment_method']

        if payment_method == 3:
            card_token_response = self.tokenize_card(data['credit_card'])
            if 'status' in card_token_response.keys():
                token_id = card_token_response['data']['id']
                transaction_data['payment_method'] = {
                    "type": PAYMENTS_METHODS[payment_method-1][1],
                    "installments": 12,
                    "token": token_id
                }
            else:
                return card_token_response

        if payment_method == 4:
            transaction_data['payment_method'] = {
                "type": PAYMENTS_METHODS[payment_method-1][1],
                "user_type": data['user_type'],
                "user_legal_id_type": data['doc_type'],
                "user_legal_id": data['doc_number'],
                "financial_institution_code": data['financial_institution_code'],
                "payment_description": f'Pago a la Garosa, ref: {data["reference"]}'
            }
            transaction_data['redirect_url'] = data['redirect_url']

        if payment_method == 5:
            transaction_data['payment_method'] = {
                "type": PAYMENTS_METHODS[payment_method - 1][1],
                "phone_number": data["phone"]
            }

        if payment_method == 6:
            transaction_data['payment_method'] = {
                "type": PAYMENTS_METHODS[payment_method - 1][1],
                "user_type": "PERSON",
                "payment_description": f'Pago a la Garosa, ref: {data["reference"]}',
                "sandbox_status": data['sandbox_status']
            }
            transaction_data['redirect_url'] = data['redirect_url']

        transaction_response = self.create_transaction(transaction_data)
        if "data" in transaction_response.keys():
            transaction_id = transaction_response['data']['id']
            transaction_status = self.get_transaction_status(
                transaction_id)

            if payment_method == 4:
                while "extra" not in transaction_status['data']['payment_method']:
                    transaction_status = self.get_transaction_status(
                        transaction_id)
                    time.sleep(1)
            return transaction_status
        else:
            return transaction_response

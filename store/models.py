import re

from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.db.models.signals import post_save, pre_save

from lagarosa.models import BaseModel

from .data import (DAYS_OF_WEEK, INGREDIENTS_CHOICES, ORDERS_TYPES,
                   PAYMENTS_METHODS, PRODUCT_TYPES)
from .signals import save_audit, send_status_order_email

User = get_user_model()


class Audit(BaseModel):
    message = models.TextField(verbose_name='Mensaje')

    class Meta:
        verbose_name = 'Auditoria'
        verbose_name_plural = 'Auditorias'
        ordering = ['-created_at']

    def __str__(self):
        return self.message


class City(BaseModel):
    """Ciudades."""

    name = models.CharField(
        max_length=150, null=False, verbose_name='Ciudad')

    class Meta:
        verbose_name = 'Ciudad'
        verbose_name_plural = 'Ciudades'
        ordering = ['name']
        indexes = [
            models.Index(fields=['name'], name='city_name_idx')
        ]

    def __str__(self):
        return self.name


class SalesPoint(BaseModel):
    """Puntos de venta."""

    def attachment_folder(self, filename):
        return '/'.join(['sales_point', 'images', str(self.id), re.sub('[^0-9a-zA-Z._]+', '', filename)])

    name = models.CharField(max_length=255, blank=False,
                            null=False, verbose_name='Nombre')
    description = models.TextField(blank=True,
                                   null=True, verbose_name='Descripción')
    address = models.CharField(max_length=255, blank=False,
                               null=False, verbose_name='Dirección')
    latitude = models.DecimalField(
        max_digits=10, decimal_places=6, verbose_name='Latitud')
    longitude = models.DecimalField(
        max_digits=10, decimal_places=6, verbose_name='Longitud')
    city = models.ForeignKey(City, related_name='cities',
                             on_delete=models.CASCADE, verbose_name='Ciudad')
    is_principal = models.BooleanField(
        verbose_name='¿Es principal?')
    thumbnail = models.ImageField(
        verbose_name='Imagen Mapa', upload_to=attachment_folder)
    day_message = models.CharField(
        max_length=200, blank=True, null=True, verbose_name='Mensaje del día')
    schedules_store = JSONField(verbose_name='Horarios', default=list)

    class Meta:
        verbose_name = 'Punto de Venta'
        verbose_name_plural = 'Puntos de Venta'
        indexes = [
            models.Index(fields=['name'], name='sales_point_name_idx')
        ]

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.is_principal:

            try:
                current_main_sales_point = SalesPoint.objects.get(
                    is_principal=True)
                if current_main_sales_point:
                    current_main_sales_point.is_principal = False
                    current_main_sales_point.save()
            except SalesPoint.DoesNotExist:
                pass
        super().save(*args, **kwargs)


post_save.connect(save_audit, sender=SalesPoint)


class Seating(BaseModel):
    """Mesas"""
    number = models.IntegerField(verbose_name='Num. Mesa')
    sale_point = models.ForeignKey(
        SalesPoint, on_delete=models.CASCADE, verbose_name='Punto de venta', related_name='seatings')

    class Meta:
        verbose_name = 'Mesa'
        verbose_name_plural = 'Mesas'

    def __str__(self):
        return f'{self.sale_point.name}: {self.number} '


class SalesPointSchedule(BaseModel):
    """Horarios de Puntos de Ventas"""

    sale_point = models.ForeignKey(
        SalesPoint, on_delete=models.CASCADE, verbose_name='Punto de venta', related_name='schedules')
    day = models.IntegerField(
        choices=DAYS_OF_WEEK, verbose_name='Día')
    start_hour = models.TimeField()
    end_hour = models.TimeField()

    class Meta:
        verbose_name = 'Horario de Punto de Venta'
        verbose_name_plural = 'Horarios de Puntos de Ventas'

    def __str__(self):
        return str(self.start_hour) + " - " + str(self.end_hour)


class ProductCategory(BaseModel):
    """Categorias de Productos."""

    category = models.CharField(
        max_length=255, blank=False, null=False, verbose_name='Categoria')
    sort_number = models.IntegerField(verbose_name='Orden en lista', default=0)

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'
        indexes = [
            models.Index(fields=['category'], name='product_category_idx')
        ]

    def __str__(self):
        return self.category


class ProductFlavor(BaseModel):
    """Sabores de Productos."""

    flavor = models.CharField(
        max_length=255, blank=False, null=False, verbose_name="Sabor")

    class Meta:
        verbose_name = 'Sabor'
        verbose_name_plural = 'Sabores'
        indexes = [
            models.Index(fields=['flavor'], name='product_flavor_idx')
        ]

    def __str__(self):
        return self.flavor


class Product(BaseModel):
    """Productos."""
    name = models.CharField(max_length=255, blank=False,
                            null=False, verbose_name='Nombre')
    description = models.CharField(max_length=255, blank=False,
                                   null=False, verbose_name='Descripción')
    product_type = models.CharField(
        max_length=2, choices=PRODUCT_TYPES)
    unit_limit = models.IntegerField(verbose_name="Unidades")
    price = models.FloatField(verbose_name="Precio")
    category = models.ManyToManyField(ProductCategory,
                                      related_name='categories', verbose_name='Categoria')
    flavor = models.ForeignKey(ProductFlavor,
                               related_name='flavors', on_delete=models.CASCADE, verbose_name='Sabor')
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, null=True, verbose_name='Usuario')
    restriction_message = models.CharField(
        max_length=300, null=True, blank=True, verbose_name='Mensaje de restricción')

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'
        indexes = [
            models.Index(fields=['name'], name='product_name_index')
        ]

    def __str__(self):
        return self.name


post_save.connect(save_audit, sender=Product)


class ProductDetail(BaseModel):
    """Detalle de Productos."""

    parent_product = models.ForeignKey(Product,
                                       related_name='parent_products', on_delete=models.CASCADE, verbose_name='Producto Padre')
    child_product = models.ForeignKey(Product,
                                      related_name='child_products', on_delete=models.CASCADE, verbose_name='Producto Hijo')

    class Meta:
        verbose_name = 'Producto Compuesto'
        verbose_name_plural = 'Productos Compuestos'

    def __str__(self):
        return self.parent_product.name


class ProductImage(BaseModel):
    """Imagenes de Productos"""

    def attachment_folder(self, filename):
        regex = re.compile('[^0-9a-zA-Z._]+')
        return '/'.join(['products/uploads/', regex.sub('_', self.product.name), regex.sub('_', filename)])

    product = models.ForeignKey(Product,
                                on_delete=models.CASCADE, verbose_name='Producto', related_name='images')
    path = models.ImageField(upload_to=attachment_folder,
                             blank=True, null=True, verbose_name='Imagen')

    def __str__(self):
        return self.path.name


class ProductIngredient(BaseModel):
    """Ingredientes de Productos"""
    product = models.ForeignKey(Product,
                                on_delete=models.CASCADE, verbose_name='Producto', related_name='ingredients')
    name = models.CharField(max_length=150, verbose_name='nombre')
    price = models.FloatField(verbose_name="Precio")
    step = models.IntegerField(verbose_name="Paso")
    choice = models.IntegerField(
        choices=INGREDIENTS_CHOICES, verbose_name='Tipo de Selección')

    class Meta:
        verbose_name = 'Ingrediente'
        verbose_name_plural = 'Ingredientes'

    def __str__(self):
        return self.name


class Inventory(BaseModel):
    """Inventarios."""

    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, verbose_name='Producto')
    sale_point = models.ForeignKey(
        SalesPoint, on_delete=models.CASCADE, verbose_name='Punto de venta')
    min_units = models.IntegerField(verbose_name='Und. minimas', default=0)
    in_stock = models.IntegerField(verbose_name='Und. en stock', default=0)
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name='Usuario')
    sort_number = models.IntegerField(verbose_name='Orden en lista')

    class Meta:
        verbose_name = 'Inventario'
        verbose_name_plural = 'Inventarios'

    def __str__(self):
        return f'{self.sale_point.name} - {self.product.name}'


post_save.connect(save_audit, sender=Inventory)


class StatusOrder(BaseModel):
    """Estados de Ordenes"""
    status = models.CharField(
        max_length=100, verbose_name='Estado')

    class Meta:
        verbose_name = "Estado Orden"
        verbose_name_plural = "Estados de Ordenes"

    def __str__(self):
        return self.status


class Order(BaseModel):
    """Ordenes"""
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, verbose_name='Usuario', related_name='orders')
    status = models.ForeignKey(
        StatusOrder, on_delete=models.CASCADE, related_name='statuses',
        default=1, verbose_name='Estado')
    sale_point = models.ForeignKey(
        SalesPoint, on_delete=models.CASCADE, verbose_name='Punto de venta')
    address = models.ForeignKey(
        "users.Address", on_delete=models.CASCADE, null=True, related_name='delivery_address',
        verbose_name='Dirección de envio')
    total_price = models.FloatField(
        verbose_name='Total')
    order_type = models.IntegerField(
        choices=ORDERS_TYPES, verbose_name='Tipo de Orden')
    delivery_price = models.FloatField(
        verbose_name='Valor Domicilio')
    delivery_name = models.CharField(
        max_length=150, verbose_name='Destinatario', null=True, blank=True)
    message = models.CharField(
        max_length=200, verbose_name='Mensaje', null=True, blank=True)
    rating = models.IntegerField(
        verbose_name='Calificación', null=True, blank=True)
    comment = models.TextField(
        verbose_name='Comentarios', null=True, blank=True)
    annotations = models.CharField(
        max_length=250, verbose_name='Observaciones', null=True, blank=True)
    payment_method = models.IntegerField(
        choices=PAYMENTS_METHODS, verbose_name='Metodo de Pago')
    order_uuid = models.UUIDField(editable=False, unique=True)
    preparation_time = models.TimeField(
        null=True, blank=True, verbose_name='Tiempo de preparación')
    delivery_time = models.TimeField(
        null=True, blank=True, verbose_name='Tiempo de envio')
    direct_client = models.CharField(
        max_length=300, null=True, blank=True, verbose_name='Cliente venta Direct')
    direct_phone = models.CharField(
        max_length=300, null=True, blank=True, verbose_name='No. Telefono Cliente venta Direct')
    direct_email = models.CharField(
        max_length=300, null=True, blank=True, verbose_name='Email cliente venta Direct')
    seating_number = models.IntegerField(
        verbose_name='Numero de mesa', default=0)
    wompi_transaction_id = models.CharField(
        verbose_name='ID Transaaccion Wompi', null=True, blank=True, max_length=250)
    pse_link = models.CharField(
        verbose_name='Link PSE', null=True, blank=True, max_length=500)
    delivery_date = models.DateTimeField(
        null=True, blank=True, verbose_name='Dia de entrega')

    class Meta:
        verbose_name = "Orden"
        verbose_name_plural = "Ordenes"

    def __str__(self):
        return str(self.id)


post_save.connect(save_audit, sender=Order)
pre_save.connect(send_status_order_email, sender=Order)


class OrderDetail(models.Model):
    product = models.ForeignKey(
        Product, verbose_name='Producto', related_name='products', on_delete=models.CASCADE)
    order = models.ForeignKey(
        Order, verbose_name='Orden', related_name='order_details', on_delete=models.CASCADE)
    quantity = models.IntegerField(
        verbose_name='Cantidad')
    unit_price = models.FloatField(
        verbose_name='Precio Unitario')
    product_configs = JSONField(verbose_name="Configuracion de productos")

    class Meta:
        verbose_name = "Detalle Orden"
        verbose_name_plural = "Detalle de Ordenes"

    def __str__(self):
        return str(self.order)


post_save.connect(save_audit, sender=OrderDetail)


class Notification(BaseModel):
    sale_point = models.ForeignKey(
        SalesPoint, verbose_name='Punto de venta', on_delete=models.CASCADE)
    order = models.ForeignKey(
        Order, verbose_name='Orden', on_delete=models.CASCADE)
    description = models.TextField(verbose_name='Descripcion')

    class Meta:
        verbose_name = "Notificación"
        verbose_name_plural = "Notificaciones"
        ordering = ['-is_active', '-created_at']

    def __str__(self):
        return self.description

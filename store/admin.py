from django.contrib import admin
from .models import (SalesPoint, City, Product, ProductCategory, ProductFlavor,
                     ProductDetail, ProductImage, ProductIngredient, Inventory,
                     SalesPointSchedule, Seating, StatusOrder, Order,
                     OrderDetail, Notification)


@admin.register(SalesPoint)
class SalesPointAdminAdmin(admin.ModelAdmin):
    list_display = ('name', 'address', 'description', 'city',
                    'is_active', 'created_at', 'is_principal', 'thumbnail', 'updated_at')
    list_filter = ('name', 'is_active')


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_active', 'created_at', 'updated_at')
    list_filter = ('name', 'is_active')


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass


@admin.register(ProductCategory)
class ProductCategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(ProductFlavor)
class ProductFlavorAdmin(admin.ModelAdmin):
    pass


@admin.register(ProductImage)
class ProductImageAdmin(admin.ModelAdmin):
    pass


@admin.register(ProductDetail)
class ProductDetailAdmin(admin.ModelAdmin):
    pass


@admin.register(ProductIngredient)
class ProductIngredientAdmin(admin.ModelAdmin):
    pass


@admin.register(Inventory)
class InventoryAdmin(admin.ModelAdmin):
    pass


@admin.register(SalesPointSchedule)
class SalesPointScheduleAdmin(admin.ModelAdmin):
    pass


@admin.register(Seating)
class SeatingAdmin(admin.ModelAdmin):
    pass


@admin.register(StatusOrder)
class StatusOrderAdmin(admin.ModelAdmin):
    pass


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    pass


@admin.register(OrderDetail)
class OrderDetailAdmin(admin.ModelAdmin):
    pass


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    pass

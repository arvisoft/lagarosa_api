#FROM python:3.8.3
#
#ENV PYTHONUNBUFFERED 1
#
#RUN mkdir lagarosa
#
#WORKDIR /lagarosa
#
#COPY . /lagarosa/
#RUN ls .
#RUN pip install -r requirements.txt
#
#EXPOSE 8000
#
#CMD python manage.py makemigrations && python manage.py migrate && python manage.py runserver 0.0.0.0:8000

FROM python:3.8.3
#Parametros del usuario que ejecuta la sintaxis
#ARG UID
#ARG GID
#RUN addgroup --gid ${GID} deploy && adduser --system --disabled-password --uid ${UID} --gid ${GID} deploy

# for uwsgi
#RUN install_packages build-essential
# for requirements.txt
RUN pip install --no-cache-dir --upgrade pip

#RUN sed -i -e 's/# es_CO.UTF-8 UTF-8/es_CO.UTF-8 UTF-8/' /etc/locale.gen && locale-gen

COPY requirements.txt .

RUN pip install --no-cache-dir uwsgi && pip install --no-cache-dir -r requirements.txt

#RUN install -d -m 0755 -o deploy -g deploy /home/deploy/web
#USER deploy
WORKDIR /lagarosa

EXPOSE 8000

COPY entrypoint.sh .
COPY server.yaml .
CMD [ "./entrypoint.sh" ]

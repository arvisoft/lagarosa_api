from django.contrib import admin
from .models import Faq, TeamMember, Config

# Register your models here.


@admin.register(Faq)
class FaqAdmin(admin.ModelAdmin):
    list_display = ('title', 'is_active', 'created_at', 'updated_at')
    list_filter = ('is_active', 'title')
    search_fields = ('description',)

@admin.register(TeamMember)
class TeamMemberAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'is_active',
                    'created_at', 'updated_at')

@admin.register(Config)
class ConfigAdmin(admin.ModelAdmin):
    pass
from rest_framework import serializers

from .models import Faq, TeamMember, Config


class TeamMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamMember
        fields = '__all__'


class FaqSerializer(serializers.ModelSerializer):
    class Meta:
        model = Faq
        fields = '__all__'


class ContactSerializer(serializers.Serializer):
    name = serializers.CharField(allow_blank=False,  min_length=3, max_length=100, error_messages={
                                 'allow_blank': 'Por favor déjanos saber tu nombre'})
    email_phone = serializers.CharField(required=True, min_length=3, max_length=100, error_messages={
        'required': 'Ingresa tu dirección de correo o número telefónico para contactarte'})
    text = serializers.CharField(required=True, error_messages={
                                 'required': 'Cuentanos tus inquietudes'})
    recaptcha_token = serializers.CharField(required=True, error_messages={
                                            'required': 'Captcha inválido'})


class ConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = Config
        fields = '__all__'
from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import FaqFrontViewSet, TeamFrontViewSet, ContactView, ConfigViewSet

router = DefaultRouter(trailing_slash=False)
router.register(r'api/info/faqs', FaqFrontViewSet, basename='info')
router.register(r'api/info/team', TeamFrontViewSet, basename='info')
router.register(r'api/info/config', ConfigViewSet, basename='info')


urlpatterns = [
    path(r'api/info/contact', ContactView.as_view(), name='info-contact'),
    path('', include(router.urls))
]

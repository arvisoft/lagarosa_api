from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from rest_framework import status, viewsets, mixins
from rest_framework.response import Response
from rest_framework.views import APIView
from lagarosa.utils import send_templated_email
from django.core.mail import EmailMessage
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from .models import Faq, TeamMember, Config
from .serializers import (ContactSerializer, FaqSerializer,
                          TeamMemberSerializer, ConfigSerializer)


class FaqFrontViewSet(viewsets.ModelViewSet):
    serializer_class = FaqSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        queryset = Faq.objects.all()
        is_active = self.request.query_params.get('is_active', None)

        if (is_active):
            return queryset.filter(is_active=is_active)

        return queryset


class TeamFrontViewSet(viewsets.ModelViewSet):
    serializer_class = TeamMemberSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        queryset = TeamMember.objects.all()
        is_active = self.request.query_params.get('is_active', None)
        if (is_active):
            return queryset.filter(is_active=is_active)

        return queryset


class ContactView(APIView):
    def post(self, request):
        serializer = ContactSerializer(data=request.data)
        if serializer.is_valid():
            email_data = {
                'site_name': get_current_site(request).name,
                'request': request,
                **serializer.validated_data
            }

            try:
                sent_email = send_templated_email(
                    subject='Quejas y Reclamos',
                    recipients=settings.EMAIL_CONTACT_TO,
                    template_path='email/contact.html',
                    data=email_data
                )

                if not isinstance(sent_email, EmailMessage):
                    return Response(serializer.validated_data, status=status.HTTP_200_OK)

                raise Exception(sent_email)
            except Exception as ex:
                return Response(data={'detail': f'There was an error \n {ex}'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ConfigViewSet(mixins.CreateModelMixin,
                    mixins.ListModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    viewsets.GenericViewSet):
    queryset = Config.objects.filter(is_active=True)
    serializer_class = ConfigSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        key = self.request.query_params.get('key', None)
        if key:
            return Config.objects.filter(is_active=True, key=key)
        return self.queryset

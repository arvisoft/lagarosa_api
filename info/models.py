import re
from django.db import models
from django.contrib.postgres.fields import JSONField
from lagarosa.models import BaseModel


class Faq(BaseModel):
    """Preguntas Frecuentes."""
    title = models.CharField(
        max_length=150, null=False, verbose_name='Pregunta')
    description = models.TextField(
        blank=False, null=False, verbose_name='Respuesta')

    class Meta:
        verbose_name = 'Pregunta Frecuente'
        verbose_name_plural = 'Preguntas Frecuentes'
        ordering = ['title', 'created_at']
        indexes = [
            models.Index(fields=['title'], name='faq_title_idx')
        ]

    def __str__(self):
        return self.title


class TeamMember(BaseModel):
    """Miembros del equipo."""

    def attachment_folder(self, filename):
        return '/'.join(['team', 'pics', str(self.pk), re.sub('[^0-9a-zA-Z._]+', '', filename)])

    first_name = models.CharField(
        max_length=50, null=False, blank=False, verbose_name='Nombres')
    last_name = models.CharField(
        max_length=50, null=False, blank=False, verbose_name='Apellidos')
    occupation = models.CharField(
        max_length=50, null=False, blank=False, verbose_name='Cargo')
    biography = models.TextField(
        verbose_name='Acerca de', blank=True, null=True, max_length=150)
    photo = models.ImageField(
        verbose_name='Foto', upload_to=attachment_folder,
        blank=False, null=False)

    class Meta:
        verbose_name = 'Equipo'
        verbose_name_plural = 'Equipo'

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        return f'{self.first_name} {self.last_name}'


class Config(BaseModel):
    """Configuraciones"""

    def attachment_folder(self, filename):
        return '/'.join(['config', 'images', str(self.id), re.sub('[^0-9a-zA-Z._]+', '', filename)])

    key = models.CharField(
        max_length=200, verbose_name="Llave")
    value = JSONField(verbose_name="Valor")
    image1 = models.ImageField(
        verbose_name='Imagen 1', upload_to=attachment_folder,
        blank=True, null=True)
    image2 = models.ImageField(
        verbose_name='Imagen 2', upload_to=attachment_folder,
        blank=True, null=True)
    image3 = models.ImageField(
        verbose_name='Imagen 3', upload_to=attachment_folder,
        blank=True, null=True)

    class Meta:
        ordering = ('id', )
        verbose_name = ("Configuracion")
        verbose_name_plural = ("Configuraciones")

    def __str__(self):
        return self.key

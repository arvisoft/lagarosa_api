import base64
import imghdr
import os
import uuid
from email.mime.image import MIMEImage
from threading import Thread

import six
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.base import ContentFile
from django.core.mail import EmailMessage
from django.template import TemplateDoesNotExist
from django.template.loader import render_to_string
from rest_framework import serializers
from store.models import Notification
from webpush import send_group_notification


def get_logo_for_email():
    logo_path = '{0}/images/logo-tuto-experts.png'.format(settings.STATIC_ROOT)
    path, filename = os.path.split(logo_path)
    logo = open(logo_path, 'rb')
    msg_logo = MIMEImage(logo.read())
    logo.close()
    msg_logo.add_header('Content-ID', '<{0}>'.format(filename))

    return msg_logo


def send_templated_email(subject, recipients, template_path, data, hidden_recipients=None):
    '''
    Envia un correo con el contenido del template dado

    subject String: Titulo del mensaje

    recipients String: Lista de destinatarios separados por coma (,)

    template_path String: Ubicación del template

    data dic: datos para pasar al template

    hidden_recipients String: lista de destinatarios ocultos separados por coma (,), default None

    Return True if operation succeeded or String.
    '''

    try:
        body_msg = render_to_string(template_path, data)

        email = EmailMessage(
            from_email='La Garosa <{}>'.format(settings.DEFAULT_FROM_EMAIL),
            subject=subject,
            to=[recipients],
            body=body_msg
        )

        if hidden_recipients:
            email.bcc = [hidden_recipients]

        email.content_subtype = 'html'
        email.mixed_subtype = 'related'
        # email.attach(get_logo_for_email())
        email.send()

        return True
    except TemplateDoesNotExist as ex:
        return str(ex.msg)


def get_request_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')

    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    return ip


class Base64ImageField(serializers.ImageField):

    def to_internal_value(self, data):

        # Check if this is a base64 string
        if isinstance(data, six.string_types):
            # Check if the base64 string is in the "data:" format
            if 'data:' in data and ';base64,' in data:
                # Break out the header from the base64 content
                header, data = data.split(';base64,')

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail('invalid_image')

            # Generate file name:
            # 12 characters are more than enough.
            file_name = str(uuid.uuid4())[:12]
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)

            complete_file_name = "%s.%s" % (file_name, file_extension, )

            data = ContentFile(decoded_file, name=complete_file_name)

        return super(Base64ImageField, self).to_internal_value(data)

    def get_file_extension(self, file_name, decoded_file):

        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension
        return extension


def create_and_send_notification(order, send=False):
    """
    Crea y envía (opcional) una notificación a los usuarios asociados al punto de venta donde se registra la orden

    Parameters
    ----------

    order: store.models.Order

    send: bool, optional
        True to dispatch the 'send' event
    """
    description = f'Nuevo Pedido de: {order.direct_client if order.direct_client else order.user.get_full_name()}'
    Notification.objects.create(
        is_active=True, description=description, sale_point=order.sale_point, order=order)

    if send:
        data = {
            'title': 'Nuevo Pedido',
            'body': description,
        }

        try:
            send_group_notification(group_name=str(
                order.sale_point.id), payload=data, ttl=1000)
        except ObjectDoesNotExist:
            pass


class EmailThread(Thread):
    def __init__(self, subject, recipients, template_path, data):
        self.subject = subject
        self.recipients = recipients
        self.template_path = template_path
        self.data = data
        Thread.__init__(self)

    def run(self):
        send_templated_email(
            self.subject, self.recipients, self.template_path, self.data)

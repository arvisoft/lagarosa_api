from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    ''' 
    Custom permission to only allow owners od an object to edit it 
    '''

    def has_object_permission(self, request, view, object):
        # Read permissions are allowed to any request
        # so we'll always allow GET, HEAD OR OPTIONS request

        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet
        return object.user == request.user or request.user.is_superuser or request.user.is_staff

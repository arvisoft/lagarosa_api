from django.db import models

class BaseModel(models.Model):
    is_active = models.BooleanField(default=True, verbose_name='¿Visible?')
    created_at = models.DateTimeField(
        auto_now_add=True, auto_now=False, editable=False, verbose_name='Creación')
    updated_at = models.DateTimeField(auto_now_add=False,
                                      auto_now=True, editable=False, verbose_name='Última actualización')

    class Meta:
        abstract = True
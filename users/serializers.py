import json

from django.contrib.auth import get_user_model, password_validation
from django.contrib.auth.models import BaseUserManager, Group as Role
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from webpush.models import Group, PushInformation, SubscriptionInfo
from webpush.views import process_subscription_data

from store.models import Order, SalesPoint
from store.serializers import OrderSerializer

from .data import PLATFORM_USERS_PROVIDERS
from .models import Address, CustomUser, Module, Subscriptor
from .utils import create_user_account

User = get_user_model()


class UserLoginSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=300, required=True)
    password = serializers.CharField(required=True, write_only=True)


class ModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Module
        fields = ('id', 'path_name', 'title', 'icon')


class SubscriptorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subscriptor
        fields = '__all__'


class RoleSerializer(serializers.ModelSerializer):
    modules = ModuleSerializer(many=True)

    class Meta:
        model = Role
        fields = '__all__'


class AuthUserSerializer(serializers.ModelSerializer):
    auth_token = serializers.SerializerMethodField()
    groups = RoleSerializer(many=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'first_name', 'phone',
                  'last_name', 'auth_token', 'is_superuser', 'groups',
                  'sale_point', 'change_password', 'is_staff')
        read_only_fields = ('id', 'is_superuser')

    def get_auth_token(self, user):
        token, created = Token.objects.get_or_create(user=user)
        return token.key


class UserRegisterSerializer(serializers.ModelSerializer):
    platform = serializers.ChoiceField(choices=PLATFORM_USERS_PROVIDERS)
    email = serializers.EmailField(required=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'password',
                  'first_name', 'last_name', 'platform')

    def validate_email(self, value):
        user = User.objects.filter(email=value)

        if user:
            raise serializers.ValidationError(
                f"the email address '{value}' is already registered.")
        return BaseUserManager.normalize_email(value)

    def validate_password(self, value):
        password_validation.validate_password(value)
        return value

    def validate_platform(self, value):
        if not [item for item in PLATFORM_USERS_PROVIDERS if item[0] == value]:
            raise serializers.ValidationError(
                f"{value} is not a valid user platform provider")
        return value


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True, required=True)
    username = serializers.CharField(read_only=True)
    total_orders = serializers.SerializerMethodField()

    class Meta:
        model = CustomUser
        fields = '__all__'
        depth = 1

    def get_total_orders(self, obj):
        return Order.objects.filter(user=obj.id).count()

    def create(self, validated_data):
        request = self.context.get('request')
        data = request.data
        try:
            sale_point_id = data.pop('sale_point')
        except:
            sale_point_id = None
        if sale_point_id:
            validated_data['sale_point'] = SalesPoint.objects.get(
                pk=sale_point_id)

        user = create_user_account(**validated_data)

        try:
            role = data.pop('role')
        except:
            role = None
        if role:
            user.groups.add(role)

        return user

    def update(self, instance, validated_data):
        request = self.context.get('request')
        data = request.data
        instance.first_name = validated_data.get(
            'first_name', instance.first_name)
        instance.last_name = validated_data.get(
            'last_name', instance.last_name)
        instance.is_active = validated_data.get(
            'is_active', instance.is_active)
        instance.email = validated_data.get(
            'email', instance.email)
        instance.username = validated_data.get(
            'email', instance.username)
        instance.sale_point = validated_data.get(
            'sale_point', instance.sale_point)
        instance.change_password = validated_data.get(
            'change_password', instance.change_password)
        try:
            instance.set_password(validated_data['password'])
        except:
            pass

        try:
            sale_point_id = data['sale_point']
        except:
            sale_point_id = None

        if sale_point_id:
            validated_data['sale_point'] = SalesPoint.objects.get(
                pk=sale_point_id)
            instance.sale_point = validated_data.get(
                'sale_point', instance.sale_point)

        try:
            role = data.pop('role')
            instance.groups.clear()
            instance.groups.add(role)
        except:
            pass

        instance.save()
        return instance


class SocialAuthSerializer(serializers.Serializer):
    user_id = serializers.CharField(required=True, max_length=300)
    access_token = serializers.CharField(required=True, max_length=500)
    provider = serializers.CharField(required=True, max_length=50)


class EmptySerializer(serializers.Serializer):
    pass


class AddressSerializer(serializers.ModelSerializer):
    user = serializers.ReadOnlyField(source='user.id')

    class Meta:
        model = Address
        fields = '__all__'


class PushSubscriptionSerializer(serializers.Serializer):
    group = serializers.CharField()
    subscription = serializers.JSONField()
    browser = serializers.CharField()
    status_type = serializers.ChoiceField(choices=[
        ('subscribe', 'subscribe'),
        ('unsubscribe', 'unsubscribe')
    ])

    def create(self, validated_data):
        request = self.context.get('request')

        subscription_data = process_subscription_data(
            json.loads(json.dumps(request.data)))
        subscription_info, si_created = SubscriptionInfo.objects.get_or_create(
            **subscription_data)

        group, g_created = Group.objects.get_or_create(
            name=request.data.pop('group', ''))

        push_information = {
            'group': group,
            'user': request.user,
            'subscription': subscription_info
        }

        push_info, pi_created = PushInformation.objects.get_or_create(
            **push_information)

        return validated_data

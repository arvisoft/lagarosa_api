from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.exceptions import ImproperlyConfigured
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action, api_view
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from webpush.models import PushInformation

from lagarosa.permissions import IsOwnerOrReadOnly

from . import serializers
from .models import Address, CustomUser, Subscriptor
from .serializers import (AddressSerializer, PushSubscriptionSerializer,
                          RoleSerializer, UserSerializer, SubscriptorSerializer)
from .utils import (create_user_account, get_and_authenticate_user,
                    retrieve_facebook_user_info, retrieve_google_user_info)
import googlemaps
gmaps = googlemaps.Client(key='AIzaSyAc-SXyORnyZmGC2ktjKwpxh_sA9gD2nfg')
# Create your views here.
User = get_user_model()


class AuthViewSet(viewsets.GenericViewSet):
    permission_classes = (AllowAny,)
    serializer_class = serializers.EmptySerializer
    serializer_classes = {
        'login': serializers.UserLoginSerializer,
        'social_signup': serializers.SocialAuthSerializer
    }

    @action(methods=['POST'], detail=False)
    def login(self, request):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            user = get_and_authenticate_user(**serializer.validated_data)
            data = serializers.AuthUserSerializer(user).data
            return Response(data=data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['POST'], detail=False)
    def social_signup(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            auth_provider = serializer.validated_data['provider']
            access_token = serializer.validated_data['access_token']
            user_auth_data = None

            if auth_provider == 'facebook':
                user_auth_data = retrieve_facebook_user_info(
                    serializer.validated_data['user_id'], access_token)
                user_reg_serializer = serializers.UserRegisterSerializer(
                    data=user_auth_data)
            elif auth_provider == 'google':
                user_auth_data = retrieve_google_user_info(access_token)
                user_reg_serializer = serializers.UserRegisterSerializer(
                    data=user_auth_data)

            if user_reg_serializer.is_valid(raise_exception=False):
                user = create_user_account(
                    **user_reg_serializer.validated_data)
                resp_data = serializers.AuthUserSerializer(user).data
                return Response(data=resp_data, status=status.HTTP_201_CREATED)

            # Check if the error is caused by duplicated email
            elif user_reg_serializer.errors.get('email') and 'already registered.' in str(user_reg_serializer.errors.get('email')[0]).lower():
                login_serializer = serializers.UserLoginSerializer(
                    data=user_auth_data)

                if login_serializer.is_valid():
                    user = get_and_authenticate_user(
                        **login_serializer.validated_data)
                    data = serializers.AuthUserSerializer(user).data
                    return Response(data=data, status=status.HTTP_200_OK)
                else:
                    return Response(login_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

            return Response(user_reg_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_serializer_class(self):
        if not isinstance(self.serializer_classes, dict):
            raise ImproperlyConfigured(
                "serializer_classes should be a dict mapping.")
        if self.action in self.serializer_classes.keys():
            return self.serializer_classes[self.action]
        return super().get_serializer_class()


class UserViewSet(viewsets.ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        is_staff = self.request.query_params.get('is_staff', None)

        if is_staff == '1':
            return CustomUser.objects.filter(
                Q(is_staff=True) | Q(is_superuser=True))
        if is_staff == '2':
            return CustomUser.objects.filter(
                Q(is_staff=False) & Q(is_superuser=False))
        return self.queryset


class RoleViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Group.objects.all()
    serializer_class = RoleSerializer
    permission_classes = [IsAuthenticated]


class SubscriptorViewSet(viewsets.ModelViewSet):
    queryset = Subscriptor.objects.all()
    serializer_class = SubscriptorSerializer


class AddressViewSet(viewsets.ModelViewSet):
    queryset = Address.objects.filter(is_active=True)
    serializer_class = serializers.AddressSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def get_queryset(self):
        user = self.request.user.id
        client = self.request.query_params.get('client', None)
        if client:
            user = CustomUser.objects.get(pk=client)

        return self.queryset.filter(user=user)

    def perform_create(self, serializer):
        user = self.request.user
        try:
            client = self.request.data['client']
        except:
            client = None
        if client:
            user = CustomUser.objects.get(pk=client)
        serializer.save(user=user)
    
    @action(methods=['post'], detail=False, permission_classes=(AllowAny,))
    def calculate_km(self, request):
        source = request.data['source']
        destination = request.data['destination']
        distance = gmaps.distance_matrix(source, destination)
        return Response(distance)

class WebPushViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = PushInformation.objects.all()
    serializer_class = PushSubscriptionSerializer

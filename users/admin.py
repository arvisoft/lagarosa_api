from django.contrib import admin
from .models import Address, Module
# Register your models here.
from django.contrib.auth import get_user_model
User = get_user_model()


class AddressAdmin(admin.ModelAdmin):
    list_display = ('user', 'description', 'address', 'state',
                    'is_principal', 'is_active', 'created_at', 'updated_at')
    list_filter = ('address', 'is_active', 'is_principal')


admin.site.register(Address, AddressAdmin)


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass


@admin.register(Module)
class ModuleAdmin(admin.ModelAdmin):
    pass

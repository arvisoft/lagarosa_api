import re

import httplib2
import requests
from django.conf import settings
from django.contrib.auth import authenticate, get_user_model
from django.core.signing import Signer
from oauth2client import client
from rest_framework import serializers


def user_profile_photo_folder(self, filename):
    return '/'.join(['users', 'pics', str(self.id), re.sub('[^0-9a-zA-Z._]+', '', str(filename, 'utf-8'))])


def get_and_authenticate_user(email, password):
    user = authenticate(username=email, password=password)

    if user is None:
        raise serializers.ValidationError(
            "Invalid credentials, please try again.")

    return user


def create_user_account(email, password, first_name='', last_name='', **extra):
    user = get_user_model().objects.create_user(
        username=email,
        email=email, password=password, first_name=first_name, last_name=last_name, **extra
    )
    return user


def retrieve_facebook_user_info(user_id, access_token):
    api_graph_url = 'https://graph.facebook.com/%s?access_token=%s&fields=id,first_name,last_name,email,birthday,name,picture{url}' % (
        user_id, access_token)

    api_Graph_rq = requests.get(api_graph_url)
    response = api_Graph_rq.json()

    if response.get('error'):
        raise serializers.ValidationError(
            'There was an error connecting with Facebook, please try again')

    response.update({'password': password_creator(user_id), 'platform': 'FB'})
    return response


def retrieve_google_user_info(auth_code):
    CLIENT_SECRET_FILE = f'{settings.BASE_DIR}/google_app_credentials.json'
    credentials = client.credentials_from_clientsecrets_and_code(
        CLIENT_SECRET_FILE,
        ['https://www.googleapis.com/auth/drive.appdata', 'profile', 'email'],
        auth_code
    )

    http_auth = credentials.authorize(httplib2.Http())
    response = {
        'id': credentials.id_token.get('sub'),
        'email': credentials.id_token.get('email'),
        'first_name': credentials.id_token.get('given_name') if not credentials.id_token.get('name') else credentials.id_token.get('name'),
        'last_name': credentials.id_token.get('family_name'),
        'password': password_creator(credentials.id_token.get('sub')),
        'platform': 'GO'
    }
    return response


def password_creator(key):
    signer = Signer()
    return signer.sign(str(key)).split(':')[1]

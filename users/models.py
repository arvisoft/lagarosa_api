from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import AbstractUser
from lagarosa.models import BaseModel
from django.contrib.auth import get_user_model
from .data import PLATFORM_USERS_PROVIDERS
from store.signals import save_audit
from django.contrib.auth.models import Group


class CustomUser(AbstractUser):

    email = models.EmailField(unique=True)
    ''' photo = models.FileField(
        'Profile Picture', upload_to=user_profile_photo_folder, blank=True, null=True) '''
    platform = models.CharField('Origin platform', max_length=2,
                                default='IN', choices=PLATFORM_USERS_PROVIDERS)
    phone = models.CharField(
        max_length=13, null=True, blank=True, verbose_name='Telefono')
    sale_point = models.ForeignKey(
        "store.SalesPoint", verbose_name='Punto de Venta', null=True, on_delete=models.CASCADE)
    change_password = models.BooleanField(
        verbose_name='Primera contaseña', default=False)


post_save.connect(save_audit, sender=CustomUser)


class Address(BaseModel):
    user = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, verbose_name='Usuario', related_name='addresses')
    description = models.CharField(
        max_length=100, null=False, blank=False, verbose_name="Descripción")
    address = models.CharField(
        max_length=100, null=False, blank=False, verbose_name="Dirección")
    is_principal = models.BooleanField(verbose_name='¿Es principal?')
    latitude = models.DecimalField(
        max_digits=10, decimal_places=6, verbose_name='Latitud')
    longitude = models.DecimalField(
        max_digits=10, decimal_places=6, verbose_name='Longitud')
    city = models.CharField(
        max_length=100, null=False, blank=False, verbose_name="Ciudad")
    state = models.CharField(
        max_length=100, null=False, blank=False, verbose_name="Departamento")
    notes = models.CharField(
        max_length=200, null=True, blank=True, verbose_name="Notas Adicionales")

    class Meta:
        verbose_name = 'Dirección'
        verbose_name_plural = 'Direcciones'
        ordering = ['address', 'created_at']

    def __str__(self):
        return self.address

    def save(self, *args, **kwargs):
        if self.is_principal:

            try:
                current_main_address = Address.objects.get(
                    is_principal=True, user=self.user)
                if current_main_address:
                    current_main_address.is_principal = False
                    current_main_address.save()
            except Address.DoesNotExist:
                pass
        super().save(*args, **kwargs)


class Module(BaseModel):
    path_name = models.CharField(verbose_name='Modulo', max_length=350)
    icon = models.CharField(verbose_name='Icono', max_length=100)
    title = models.CharField(verbose_name='Titulo', max_length=100)
    group = models.ForeignKey(
        Group, verbose_name='Grupo', on_delete=models.CASCADE, related_name='modules')

    class Meta:
        verbose_name = 'Modulo'
        verbose_name_plural = 'Modulos'

    def __str__(self):
        return f'{self.group.name} - {self.path_name}'


class Subscriptor(BaseModel):
    email = models.EmailField(unique=True, verbose_name='Correo Electronico')

    class Meta:
        verbose_name = 'Suscriptor'
        verbose_name_plural = 'Suscriptores'

    def __str__(self):
        return self.email

from django.conf.urls import url
from django.urls import include, path
from rest_framework import routers

from .views import (AddressViewSet, AuthViewSet, RoleViewSet, UserViewSet,
                    WebPushViewSet, SubscriptorViewSet)

router = routers.DefaultRouter(trailing_slash=False)
router.register('api/auth', AuthViewSet, basename='auth')
router.register(r'api/user/address', AddressViewSet, basename='address')
router.register(r'api/users', UserViewSet, basename='users')
router.register(r'api/user/roles', RoleViewSet, basename='roles')
router.register(r'api/webpush/', WebPushViewSet, basename='webpush')
router.register(r'api/user/subscriptors',
                SubscriptorViewSet, basename='subscriptors')

urlpatterns = [
    path('', include(router.urls))
]
